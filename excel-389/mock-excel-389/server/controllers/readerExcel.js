// const reportModel = require('../models/reportModel');
const { readerExcel: readerExcel, readerFileHash } = require('./utils');
const fs = require('fs')
const path = require('path')


function readAllFiles(base, cb) {

    let files = fs.readdirSync(base);
    files.forEach(file => {
        // 获取当前文件信息-对象
        let stat = fs.statSync(path.join(base, file));
        // 如果当前文件类型为file的话
        if (stat.isFile()) {
            const fileNameAttr = file.split('.').pop();
            if (['xlsx'].includes(fileNameAttr.toLowerCase())) {
                cb && cb(file, base)
            }
        } else {
            // 目录类型
            readAllFiles(path.join(base, file), cb);
        }
    });
}




//读取所有的excel文件

async function readerAllExcelObjs(ctx) {

    // const result = await reportModel.findAllReport();
    let result = [];
    readAllFiles(path.resolve('./public/files'), function (fileName, path) {
        result.push({ path: path + '/' + fileName, name: fileName })
    });


    if (result && result.length > 0) {
        console.log('开始读取报表')
        const readerResult = []
        result.forEach(async (v, i) => {
            debugger
            console.log(`开始读第${i}`)
            // let fileHash = await readerFileHash(v.path)
            // let analysisResult = analysisIfComputer(v.name);
            let res = readerExcel(v.path);
            console.log(`读取完第${i}`)
            readerResult.push({
                excelData: res.datas,
                name: v.name,
                id: i
            })
        })
        
        console.log('读取结束')
        return readerResult
    }
}


const analysisIfComputer = (item) => {
    debugger
    const { path, fileName } = item;
    let analysisResult = {}
    let fileResult = [];
    readAllFiles(path.resolve('./public/xlsxResult'), function (fileName, path) {
        fileResult.push({ path: path + '/' + fileName, name: fileName })
    });
    const target = fileResult.find(v => v.name)
}


module.exports = {
    readerAllExcelObjs
};