const xlsx = require('xlsx');
const jwt = require('jsonwebtoken')
const CERT = 'mySecret';

const readerExcel = (filePath) => {
    const datas = [];
    const workbook = xlsx.readFile(filePath);
    const sheetNames = workbook.SheetNames; // 返回 ['sheet1', ...]
    for (const sheetName of sheetNames) {
        const worksheet = workbook.Sheets[sheetName];
        const data = xlsx.utils.sheet_to_json(worksheet);
        datas.push(data);
    }
    return {
        status: true,
        datas
    };
}

const generateToken = user => {
    // sign 是同步的
    return jwt.sign(user, CERT, {
        expiresIn: 1200 // 单位是秒
    })
}
const validAuth = (ctx, next) => {
    return new Promise((resolve, reject) => {
        const token = ctx.req.headers.authorization;
        if (token) {
            jwt.verify(token, CERT, function (err, decoded) {
                if (err) {
                    if (err.name === 'TokenExpiredError') {
                        resolve({ status: false, msg: '认证码失效，请重新登录!', token: 'loseefficacy' })
                    } else {
                        resolve({ status: false, msg: '认证码失效，认证失败!', token: 'error' })
                    }
                } else {
                    resolve({ status: true, msg: '认证码成功', token: 'efficacy', data: decoded })
                }
            })
        } else {
            resolve({ status: false, msg: '没有token信息', token: 'none' })
        }
    })
}



const readerFileHash = (file) => {
    const SparkMD5 = require('spark-md5');
    return new Promise(resolve => {
        var fileReader = new FileReader(),
            blobSlice = File.prototype.mozSlice || File.prototype.webkitSlice || File.prototype.slice,
            chunkSize = 2097152,
            // read in chunks of 2MB
            chunks = Math.ceil(file.size / chunkSize),
            currentChunk = 0,
            spark = new SparkMD5();

        fileReader.onload = function (e) {
            spark.appendBinary(e.target.result); // append binary string
            currentChunk++;

            if (currentChunk < chunks) {
                loadNext();
            }
            else {
                console.log("finished loading");
                resolve(spark.end())
            }
        };

        function loadNext() {
            var start = currentChunk * chunkSize,
                end = start + chunkSize >= file.size ? file.size : start + chunkSize;

            fileReader.readAsBinaryString(blobSlice.call(file, start, end));
        };
        loadNext();
    })
}


module.exports = {
    readerExcel,
    generateToken,
    validAuth,
    readerFileHash
}